import numpy as np
from PIL import Image, ImageDraw
import time
from datetime import datetime
import imageio
import os
import datetime
from progressbar import ProgressBar
import cv2
import re
import random
import numpy as np
import itertools
from statistics import mean
import natsort
import matplotlib.pyplot as plt


def displayImage(image):
    p = Image.fromarray(np.reshape((image*255.0), (480, 1024)))
    p.show()

def displayQval(image):
    p = Image.fromarray(np.reshape(np.multiply(image,255.0), (29, 63)))
    p.show()

def showImage(ImgArray): 
    # takes in normalized input of image array
    Image.fromarray(np.uint8(ImgArray*255.0)).show()

def concatArrays( arr1, arr2): 
    # takes in array with dimension of the array is [1, 480, 1024, 1]
    # outputs an array with dimenstion of array [1, 480, 1024, 1] 
    arr1= np.vstack([arr1, np.zeros(arr1.shape[1])+1])       
    concat_arr = np.vstack([arr1, arr2])
    concat_arr = np.pad(concat_arr, ((1, 1), (1, 1)), mode='constant', constant_values=1)
    return concat_arr 

def saveImage(ImgArray, path):
    # takes in normalized input of image array with dimensions [480, 1024]
    # saves the image in the given path
    Image.fromarray(np.uint8(ImgArray*255.0)).save(path)

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    import re
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]

def MakeVideo( image_folder, video_name):
    import cv2
    import os
    images = [img for img in (os.listdir(image_folder)) if img.endswith(".png")]
    images.sort(key=natural_keys)
    frame = cv2.imread(os.path.join(image_folder, images[0]))
    height, width, layers = frame.shape
    
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    video = cv2.VideoWriter(video_name, fourcc, 10, (width,height))
    
    for image in images:
        video.write(cv2.imread(os.path.join(image_folder, image)))
    
    cv2.destroyAllWindows()
    video.release()

def normalize(array): 
    # takes in array with dimension of the array is [480, 1024]
    # returns an array with dimensios [480, 1024]
    if np.max(array) - np.min(array) == 0:
        norm_mat = np.zeros((array.shape[0], array.shape[1]))
    else:
        norm_mat = (array - np.min(array))/(np.max(array)-np.min(array))   
    return norm_mat

def VisualArray( Q_prevFrame, R_nextFrame): 
    # takes in array with dimensions [1, 480, 1024, 1]
    # returns q1 with dimensions [480, 1024]
    # returns r2 with dimensions [480, 1024]
    # returns img_arr with dimensions [961, 1024]   
    Qval_temp  = arr_resize(Q_prevFrame)
    R_temp = arr_resize(R_nextFrame)
    q1 = normalize(np.array(Qval_temp).clip(min=0))
    r2 = normalize(np.array(R_temp).clip(min=0))
    img_arr = concatArrays(r2, q1)
    return q1, r2, img_arr

def arr_resize(arr):
    # takes in array with dimensions [1, 39, 63, 1]
    # returns an array  with dimensions [480, 1024]
    q1  = np.reshape(arr, (arr.shape[0], arr.shape[1]))
    #print('q1_shape', q1.shape)
    q1 = Image.fromarray(q1)
    q1_resize = q1.resize((1024, 640))
    q1_arr = np.array(q1_resize)
    return q1_arr

def twoD_to_threeD(arr):
    arr1 = np.reshape(arr, (arr.shape[0], arr.shape[1], 1))
    arr_3D = np.append(arr1, arr1, axis = 2)
    arr_3D = np.append(arr_3D, arr1, axis = 2)
    return arr_3D

def get_one_bbox(Qval,npops,width_limits=[0,160],height_limits=[0,200]):
    Qval = arr_resize(Qval)
    qval = normalize(np.array(Qval).clip(min=0))
    frame_thresh = np.uint8(qval*255.0)
    ret,thresh = cv2.threshold(frame_thresh,90,200,0)
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    bbox_density_sum = []
    flag = 0
    for contour in contours:
        bbox = cv2.boundingRect(contour)
        x,y,w,h = bbox
        if((width_limits[0]<w<width_limits[1]) and (height_limits[0]<h<height_limits[1])):
            bbox_density_sum.append(sum(sum(Qval[y:y+h,x:x+w])))
            flag = 1
        else:
            bbox_density_sum.append(-99)
    
    # get the index with max sum
    if flag == 0:
        return None,None,None
    
    srtd_dens=sorted(bbox_density_sum,reverse=True)
    initl=len(srtd_dens)
    if(len(srtd_dens)<npops):
        srtd_dens[len(srtd_dens):npops]=[srtd_dens[0]]*(npops-len(srtd_dens))
    else:
        srtd_dens=srtd_dens[:npops]
    max_atten = max(bbox_density_sum)
    max_atten_index = bbox_density_sum.index(max_atten)
    frt=[]
    for i in range(len(srtd_dens)):
        max_atten_indexx=bbox_density_sum.index(srtd_dens[i])
        frt.append(contours[max_atten_indexx])
    kkk=np.array(frt)
    return [kkk],[[hierarchy[0][max_atten_index]]], list(cv2.boundingRect(contours[max_atten_index]))

def area_of_overlap(bbox_pred, bbox_orig):
    predxmin, predymin, predw, predh = bbox_pred[0], bbox_pred[1], bbox_pred[2], bbox_pred[3]
    pred_coordinates = list((itertools.product(range(predxmin, predxmin + predw), range(predymin, predymin + predh))))
    pred_coordinates = set(pred_coordinates)

    origxmin, origymin, origw, origh = bbox_orig[0], bbox_orig[1], bbox_orig[2], bbox_orig[3]
    orig_coordinates = list((itertools.product(range(origxmin, origxmin + origw), range(origymin, origymin + origh))))
    orig_coordinates = set(orig_coordinates)

    intersection_area = len(orig_coordinates.intersection(pred_coordinates))
    union_area = len(orig_coordinates.union(pred_coordinates))

    detected_box = len(pred_coordinates)
    original_box = len(orig_coordinates)

    if detected_box != 0:
        recall = intersection_area / detected_box
    else:
        recall = 0

    if original_box != 0:
        precision = intersection_area / original_box
    else:
        precision = 0

    if union_area != 0:
        iou = intersection_area / union_area
    else:
        iou = 0

    return iou, precision, recall

def accuracyy(Q, R):
    i_pred = []
    iu = []
    precision = []
    recall = []

    for k in range(len(Q)):
        qval, rwd, img_arr = VisualArray(Q[k], R[k])
        _, rwd_attn, _ = VisualArray(Q[k], R[k])
        rwd_thresh = np.uint8(rwd * 255.0)
        _, thresh_rwd = cv2.threshold(rwd_thresh, 80, 200, 0)
        contours_rwd, hierarchy_rwd = cv2.findContours(thresh_rwd, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours, hierarchy, _ = get_one_bbox(qval,len(contours_rwd))
        for i in range(len(contours_rwd)):
            if(contours == None or hierarchy == None):
                x, y, w, h = 0,0,0,0
            else:
                x, y, w, h = cv2.boundingRect(contours[0][i])

            try:
                x1, y1, w1, h1 = cv2.boundingRect(contours_rwd[i])
            except IndexError:
                x1, y1, w1, h1 = 0,0,0,0
            bbox_pred = [x,y,w,h]
            bbox_orig = [x1,y1,w1,h1]
            if (bbox_pred == None):
                i_pred.append(0)
                iu.append(0)
                precision.append(0)
                recall.append(0)
            else:
                iou, prec, rec = area_of_overlap(bbox_pred, bbox_orig)
                iu.append(iou)
                precision.append(prec)
                recall.append(rec)
                i_score = iou
                i_pred.append(i_score)
        k += 1
    # To check how many predictions are outside specified width and height

    return iu, i_pred, mean(i_pred), precision, mean(precision), recall, mean(recall)

def BoundingBox(Q_prevFrame, R_nextFrame ,val_image_path, valid_list,  Qval_path, save_path):
    video_path = save_path  + '/video'
    if not os.path.exists(video_path):
        os.makedirs(video_path)
    pbar_acc = ProgressBar()
    for k in pbar_acc(range(len(Q_prevFrame))):
        road_img = cv2.imread(val_image_path + '/' + str(valid_list[k]))
        qval, rwd, img_arr = VisualArray(Q_prevFrame[k], R_nextFrame[k])
        _, rwd_attn, _ = VisualArray(Q_prevFrame[k], R_nextFrame[k])
        #contours, hierarchy, _ = get_one_bbox(qval)
        rwd_thresh = np.uint8(rwd * 255.0)
        _, thresh_rwd = cv2.threshold(rwd_thresh, 80, 200, 0) # SECOND PARAMETR IS THE MOST IMPORTANT FOR GIVING AWAY THE CNTS
        contours_rwd, hierarchy_rwd = cv2.findContours(thresh_rwd, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours, hierarchy, _ = get_one_bbox(qval,len(contours_rwd))
        qval_3D = twoD_to_threeD(qval * 255.0)
        rwd_3D = twoD_to_threeD(rwd * 255.0)
        rwd_attn_3D = twoD_to_threeD(rwd_attn * 255.0)

        for i in range(len(contours_rwd)):
            if(contours == None or hierarchy == None):
                x, y, w, h = 0,0,0,0
            else:
                x, y, w, h = cv2.boundingRect(contours[0][i])

            try:
                x1, y1, w1, h1 = cv2.boundingRect(contours_rwd[i])
            except IndexError:
                x1, y1, w1, h1 = 0,0,0,0
            cv2.rectangle(road_img, (x1, y1), (x1+w1, y1+h1), (255, 0, 0), 2)
            cv2.rectangle(road_img, (x, y), (x+w, y+h), (0, 0, 255), 2)
            cv2.rectangle(qval_3D, (x, y), (x+w, y+h), (0, 0, 255), 2)
            cv2.rectangle(rwd_3D, (x1, y1), (x1+w1, y1+h1), (255, 0, 0), 2)
        final_img = np.vstack([road_img, qval_3D, rwd_attn_3D])
        cv2.imwrite(save_path + '/' + str(valid_list[k]), final_img)
        cv2.imwrite(video_path + '/' + str(valid_list[k]), road_img)
        cv2.destroyAllWindows()

    MakeVideo(save_path, save_path + '/video.avi')
    MakeVideo(video_path, video_path + '/video.avi')
    
def sorted_aphanumeric(data):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

def random_locs(batch_size, w = 100, h = 80):
    x_w = np.zeros([batch_size], dtype = int) + w
    y_h = np.zeros([batch_size], dtype = int) + h
    x = np.random.uniform(0,1024,batch_size).astype(int)
    y = np.random.uniform(0,640,batch_size).astype(int)
#    x,y = random.sample(range(0, 1024), batch_size),random.sample(range(0, 640), batch_size)
    locs = [i for i in zip(x,y,x_w,y_h)]
    return locs

def get_plots(max_epochs, metrics, labels, title, plot_save_path):
    for metric, label in zip(metrics, labels):
        plt.plot(range(max_epochs), metric, label=label)
        plt.title(title)
    plt.xlabel('Epochs')
    plt.legend(loc='best')
    file_name = title + ".png"
    plt.savefig(plot_save_path + file_name)
    plt.close()



