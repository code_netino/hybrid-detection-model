from torchvision import transforms
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import torch
import numpy as np
import os
import timeit
from tqdm import tqdm
from statistics import mean
import csv
from model import AlexNet1,AlexNet2,Identity
import dataloader as dtl
import utilspt as utils
import glob
import matplotlib.pyplot as plt
from dataloader import split_dataset,Mpboxes
from torch.utils.model_zoo import load_url as load_state_dict_from_url

def traffic_signal_data_multiple_test(imgs_path):

    imgl=os.listdir(imgs_path)
    main_dict={}
    for i in range(len(imgl)):
        main_dict[imgl[i]]=[]   
    return main_dict

def train_model(train_data,val_data,test_data,lr,num_epochs,train_list,valid_list,test_list):    
    test_mean_pre,test_mean_re,valid_mean_pre,valid_mean_re,mean_acc,test_loss,valid_loss,train_loss=[],[],[],[],[],[],[],[]
    train_loss_total,valid_loss_total,test_loss_total = 0,0,0

    # Loading of Data
    train_loader=DataLoader(dataset=train_data,batch_size=32,shuffle=False,num_workers=4)
    val_loader=DataLoader(dataset=val_data,batch_size=32,shuffle=False,num_workers=4)
    test_loader=DataLoader(dataset=test_data,batch_size=32,shuffle=False,num_workers=4)

    # Defining the Alexnet as the base for the 2 Models
    detect_rgb=AlexNet1()
    detect_attention=AlexNet2()
    detect_rgb.load_state_dict(state_dict)
    detect_rgb.avgpool=Identity()
    detect_rgb.channel_reduce=nn.Conv2d(256,1,kernel_size=1)
    detect_rgb.channel_reduce1=nn.Conv2d(2,1,kernel_size=1)
    detect_attention.load_state_dict(state_dict)
    detect_attention.avgpool=Identity()
    detect_attention.channel_reduce=nn.Conv2d(256,1,kernel_size=1)

    # Pushing the Models to GPU
    detect_rgb.to(device)
    detect_attention.to(device)

    # Defining the optimzers for both the Models
    optimizer_1 = torch.optim.Adam(detect_rgb.parameters(), lr=lr, weight_decay=1e-5)
    optimizer_2 = torch.optim.Adam(detect_attention.parameters(), lr=lr, weight_decay=1e-5)
    print('Total params: %.2fM' % (sum(p.numel() for p in detect_rgb.parameters()) / 1000000.0))

    # Training process
    trainvaltest_loaders = {'train': train_loader, 'val': val_loader,'test':test_loader}
    trainval_sizes = {x: len(trainvaltest_loaders[x].dataset) for x in ['train', 'val','test']}
    for epoch in range(0, num_epochs):
        for phase in ['train','val','test']:
            start_time = timeit.default_timer()

            running_loss = 0.0
            running_corrects = 0.0

            if phase == 'train':
                detect_rgb.train()
                detect_attention.train()
                optimizer_1.zero_grad()
                optimizer_2.zero_grad()
            else:
                detect_rgb.eval()
                detect_attention.eval()
            Q_valid,R_valid=[],[]

            for inputs,rwds,loc,attimgs,attimgs_next,next_image in tqdm((trainvaltest_loaders[phase])):
                inputs,rwds,loc,attimgs,attimgs_next,next_image=inputs.float(),rwds.float(),loc.float(),attimgs.float(),attimgs_next.float(),next_image.float()

                # Casting the data as Torch Varaibles
                inputs = Variable(inputs, requires_grad=True).to(device)
                attimgs = Variable(attimgs, requires_grad=True).to(device)
                next_image = Variable(next_image, requires_grad=False).to(device)
                attimgs_next = Variable(attimgs_next, requires_grad=False).to(device)
                rwds= Variable(rwds,requires_grad=False).to(device)

                if phase == 'train':  
                    # O/P from both the Models for the next(t+1) time step
                    outputsrgb_next = detect_rgb(next_image)
                    outputsatt_next= detect_attention(attimgs_next)   

                    # Concatenation of O/P from the models for the next(t+1) time step
                    cat_op_next=torch.cat((outputsrgb_next,outputsatt_next),1)  

                    # 1X1 Conv on the concat O/P
                    cat_op_next=detect_rgb.channel_reduce1(cat_op_next) 
                    outputsrgb = detect_rgb(inputs)
                    outputsatt= detect_attention(attimgs)

                    # Concatenation of O/P from the models for the current(t) time step
                    cat_op=torch.cat((outputsrgb,outputsatt),1)
                    cat_op=detect_rgb.channel_reduce1(cat_op)

                    # TDE Loss
                    tr_loss=torch.mean((rwds+gamma*cat_op_next-cat_op)**2)
                    train_loss_total+=tr_loss.item()
                
                # Validation and Testing
                elif phase =='val':
                    with torch.no_grad():
                        # O/P from both the Models for the next(t+1) time step
                        outputsrgb_next = detect_rgb(next_image)
                        outputsatt_next= detect_attention(attimgs_next)

                        # Concatenation of O/P from the models for the next(t+1) time step 
                        cat_op_next=torch.cat((outputsrgb_next,outputsatt_next),1)  

                        # 1X1 Conv on the concat O/P(t+1)
                        cat_op_next=detect_rgb.channel_reduce1(cat_op_next)
                        
                        # O/P from both the Models for the current(t) time step
                        outputsrgb = detect_rgb(inputs)
                        outputsatt= detect_attention(attimgs)

                        # Concatenation of O/P from the models for the current(t) time step
                        cat_op=torch.cat((outputsrgb,outputsatt),1)

                        # 1X1 Conv on the concat O/P(t)
                        cat_op=detect_rgb.channel_reduce1(cat_op)

                        # TDE Loss
                        vl_loss=torch.mean((rwds+gamma*cat_op_next-cat_op)**2)
                        valid_loss_total+=vl_loss.item()
                else:
                    with torch.no_grad():
                        # O/P from both the Models for the next(t+1) time step
                        outputsrgb_next = detect_rgb(next_image)
                        outputsatt_next= detect_attention(attimgs_next)

                        # Concatenation of O/P from the models for the next(t+1) time step
                        cat_op_next=torch.cat((outputsrgb_next,outputsatt_next),1)  

                        # 1X1 Conv on the concat O/P(t+1)
                        cat_op_next=detect_rgb.channel_reduce1(cat_op_next)

                        # O/P from both the Models for the current(t) time step
                        outputsrgb = detect_rgb(inputs)
                        outputsatt= detect_attention(attimgs)

                        # Concatenation of O/P from the models for the current(t) time step
                        cat_op=torch.cat((outputsrgb,outputsatt),1)

                        # 1X1 Conv on the concat O/P(t)
                        cat_op=detect_rgb.channel_reduce1(cat_op) 

                        # TDE Loss
                        tst_loss=torch.mean((rwds+gamma*cat_op_next-cat_op)**2)
                        test_loss_total+=tst_loss.item()
                
                # Updating the Parameters
                if phase == 'train':
                    tr_loss.backward()
                    optimizer_1.step()
                    optimizer_2.step()
                    optimizer_1.zero_grad()
                    optimizer_2.zero_grad() 
                    if(len(cat_op)!=bs):
                        pass
                    else:
                       cat_op=cat_op.detach().data.cpu().numpy()
                       rwds=rwds.detach().data.cpu().numpy()
                       Q_valid.append(np.reshape(cat_op, [bs, 39, 63, 1]))
                       R_valid.append(np.reshape(rwds, [bs, 39, 63, 1]))
                else:
                    if(len(cat_op)!=bs):
                        pass
                    else:
                       cat_op=cat_op.detach().data.cpu().numpy()
                       rwds=rwds.detach().data.cpu().numpy()
                       Q_valid.append(np.reshape(cat_op, [bs, 39, 63, 1]))
                       R_valid.append(np.reshape(rwds, [bs, 39, 63, 1]))

            # Calling the Bounding Box function across the O/P of all 3 phases(Train,Val and Test)
            if((len(Q_valid)!=0) and (phase == 'train') and (epoch == (num_epochs-1))):
                Qs_valid,Rs_valid = np.array(Q_valid),np.array(R_valid)
                Qs_valid = np.reshape(Qs_valid, [Qs_valid.shape[0]*Qs_valid.shape[1], Qs_valid.shape[2], Qs_valid.shape[3], Qs_valid.shape[4]])
                Rs_valid = np.reshape(Rs_valid, [Rs_valid.shape[0]*Rs_valid.shape[1], Rs_valid.shape[2], Rs_valid.shape[3], Rs_valid.shape[4]])
                utils.BoundingBox(Qs_valid, Rs_valid, train_path, train_list, Qval_path+'/train', save_path+'/train')
            elif((len(Q_valid)!=0) and (phase == 'val') and (epoch == (num_epochs-1))):
                Qs_valid,Rs_valid = np.array(Q_valid),np.array(R_valid) 
                Qs_valid = np.reshape(Qs_valid, [Qs_valid.shape[0]*Qs_valid.shape[1], Qs_valid.shape[2], Qs_valid.shape[3], Qs_valid.shape[4]])
                Rs_valid = np.reshape(Rs_valid, [Rs_valid.shape[0]*Rs_valid.shape[1], Rs_valid.shape[2], Rs_valid.shape[3], Rs_valid.shape[4]])
                utils.BoundingBox(Qs_valid, Rs_valid, train_path, valid_list, Qval_path+'/val', save_path+'/val')
            elif((len(Q_valid)!=0) and (phase == 'test') and (epoch == (num_epochs-1))):
                Qs_valid,Rs_valid = np.array(Q_valid),np.array(R_valid)
                Qs_valid = np.reshape(Qs_valid, [Qs_valid.shape[0]*Qs_valid.shape[1], Qs_valid.shape[2], Qs_valid.shape[3], Qs_valid.shape[4]])
                Rs_valid = np.reshape(Rs_valid, [Rs_valid.shape[0]*Rs_valid.shape[1], Rs_valid.shape[2], Rs_valid.shape[3], Rs_valid.shape[4]])
                utils.BoundingBox(Qs_valid, Rs_valid, train_path, test_list, Qval_path+'/test', save_path+'/test')
            else:
                pass
            
            # Calculating the Evaluation Metrics across the O/P of all 3 phases(Train,Val and Test)
            if phase =='train':
                Q_valid,R_valid = np.array(Q_valid),np.array(R_valid)
                Q_valid = np.reshape(Q_valid, [Q_valid.shape[0]*Q_valid.shape[1], Q_valid.shape[2], Q_valid.shape[3], Q_valid.shape[4]])
                R_valid = np.reshape(R_valid, [R_valid.shape[0]*R_valid.shape[1], R_valid.shape[2], R_valid.shape[3], R_valid.shape[4]])
                a,b=Q_valid,R_valid
                train_list=train_list[:len(a)]
                intersection_over_union, pred_score, acc, precision, mean_precision_train, recall, mean_recall_train = utils.accuracyy(a, b)
                print('Epoch:', epoch, ' ', "Train_mAP", mean_precision_train * 100,"Train_mAR", mean_recall_train * 100, "acc", acc * 100)
                train_loss.append(train_loss_total)
            elif phase == 'val':
                Q_valid,R_valid = np.array(Q_valid),np.array(R_valid)
                Q_valid = np.reshape(Q_valid, [Q_valid.shape[0]*Q_valid.shape[1], Q_valid.shape[2], Q_valid.shape[3], Q_valid.shape[4]])
                R_valid = np.reshape(R_valid, [R_valid.shape[0]*R_valid.shape[1], R_valid.shape[2], R_valid.shape[3], R_valid.shape[4]])
                a,b=Q_valid,R_valid
                valid_list=valid_list[:len(a)]
                intersection_over_union, pred_score, acc, precision, mean_precision_valid, recall, mean_recall_valid = utils.accuracyy(a, b)
                print('Epoch:', epoch, ' ', "Valid_mAP", mean_precision_valid * 100,"Valid_mAR", mean_recall_valid * 100, "acc", acc * 100)
                valid_mean_pre.append(mean_precision_valid * 100)
                valid_mean_re.append(mean_recall_valid * 100)
                valid_loss.append(valid_loss_total)
            else:
                Q_valid,R_valid = np.array(Q_valid),np.array(R_valid)
                Q_valid = np.reshape(Q_valid, [Q_valid.shape[0]*Q_valid.shape[1], Q_valid.shape[2], Q_valid.shape[3], Q_valid.shape[4]])
                R_valid = np.reshape(R_valid, [R_valid.shape[0]*R_valid.shape[1], R_valid.shape[2], R_valid.shape[3], R_valid.shape[4]])
                a,b=Q_valid,R_valid
                test_list=test_list[:len(a)]
                intersection_over_union, pred_score, acc, precision, mean_precision, recall, mean_recall = utils.accuracyy(a, b)
                print('Epoch:', epoch, ' ', "Test_mAP", mean_precision * 100,"Test_mAR", mean_recall * 100, "acc", acc * 100)
                mean_acc.append(acc * 100)
                test_mean_pre.append(mean_precision * 100)
                test_mean_re.append(mean_recall * 100)
                test_loss.append(test_loss_total)
    
    # Plotting the metrics
    stop_time = timeit.default_timer()
    print("Execution time: " + str(stop_time - start_time) + "\n")
    title = "Test_mAP_mAR"
    metrics = [test_mean_pre, test_mean_re]
    labels = ["Test_mAP", "Test_mAR"]
    utils.get_plots(nEpochs, metrics, labels, title, plot_save_path)

    title = "Valid_mAP_mAR"
    metrics = [valid_mean_pre, valid_mean_re]
    labels = ["Valid_mAP", "Valid_mAR"]
    utils.get_plots(nEpochs, metrics, labels, title, plot_save_path)

    title = "Training and Valdation _loss"
    metrics = [train_loss, valid_loss]
    labels = ["Train_loss", "Valid_loss"]
    utils.get_plots(nEpochs, metrics, labels, title, plot_save_path)

    title = "Training, Valdation mAP, and mAR"
    metrics = [train_loss, valid_mean_pre, valid_mean_re]
    labels = ["Training loss", "Validation mAP", "Validation mAR"]
    utils.get_plots(nEpochs, metrics, labels, title, plot_save_path)

def main():
    dset_all=Mpboxes(images_path,gt_path,16)
    data_dict = traffic_signal_data_multiple_test(images_path)
    img_names=list(data_dict.keys())
    train_list=img_names[:int(0.7*len((img_names)))]
    val_list=img_names[int(0.7*len((img_names))):int(0.9*len((img_names)))]
    test_list=img_names[int(0.9*len((img_names))):]
    train_dataset_all,valid_dataset_all,test_dataset_all=split_dataset(dset_all,[len(train_list),len(val_list),len(test_list)])
    lr = 0.00015
    train_model(train_dataset_all,valid_dataset_all,test_dataset_all,lr,nEpochs,train_list,val_list,test_list)
if __name__ == "__main__":
    device = torch.device("cuda:1" if torch.cuda.is_available() else "cpu")
    final_path=r'F:\Sujith\results_pyt'
    Qval_path = final_path + '/' + 'Qval'
    if not os.path.exists(Qval_path):
        os.makedirs(Qval_path)
    save_path = final_path + '/' + 'Pred'
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    plot_save_path=r'F:\Sujith\results_pyt\plots'
    print("Device being used:", device)
    nEpochs = 5
    gamma=0.4
    bs=32
    model_urls = {'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth'}
    state_dict = load_state_dict_from_url(model_urls['alexnet'],progress=True)
    images_path=r'F:\Sujith\tlr_data_big\images'
    train_path=valid_path=test_path=images_path
    gt_path=r'F:\Sujith\tlr_data_big\Checking-data.csv'
    main()
