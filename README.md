# Hybrid Model(Object Detection)

Hybrid Model is an object detection model based on reinforcement learning for its processing. The hybrid model is built on top of 2 AlexNet based networks for its output, the first network takes the RGB frame at the current time step(t) as the input, the other network takes a soft attention version of the original Image(Applying a Gaussian over the Value channel by converting the RGB image to a HSV image). The output from both the networks are then concatenated together and dimensionally reduced across the channel through 1*1 convolutions to the get the final output Q_(t) at the current time step "t".The same process is repeated for the input frame at the next time step(t+1) to get the final output Q_(t+1).The learning rule for this Model is governed by the temporal difference error(TDE) where the reward matrix is based upon the degree of overlap between the RGB image and the soft attention Image.

- dataloader.py - Implementation of the dataloader by which the data(frames)for the computation of the model is given. 
- model.py - Implementation of the Hybrid model architecture.
- train.py - Implementation of the detection script which includes the training and the evaluation part.
- utilspt.py - Implementation of Preprocessing and other utility scripts.
- model_arch.png - Architecture of the Hybrid Detection Model.

Some of the libraries used for this Implementation:-

- Pytorch 
- Numpy
- OpenCv-Python
- PIL
- Matplotlib

The Architecture of the Hybrid Detection Model is given below:-

![Hybrid Detection Model Architecture](model_arch.PNG)

### Credits

This was a project done in collabration with Continental Automotive. The road traffic data for this project was given by Continental Automotive.
