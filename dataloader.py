import torch
import os
from PIL import Image
from torch.utils.data.dataset import Dataset
import numpy as np
import cv2
from torch._utils import _accumulate
import itertools
from statistics import mean
from torch.utils.data import DataLoader
import natsort
import matplotlib.pyplot as plt
import csv
import glob

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

def traffic_signal_data_multiple_test(imgs_path):

    imgl=os.listdir(imgs_path)
    #print(imgl)
    main_dict={}
    for i in range(len(imgl)):
        main_dict[imgl[i]]=[]   
    return main_dict

def arr_resize(arr):
    # takes in array with dimensions [1, 29, 63, 1]
    # returns an array  with dimensions [480, 1024]
    q1  = np.reshape(arr, (arr.shape[0], arr.shape[1]))
    #print('q1_shape', q1.shape)
    q1 = Image.fromarray(q1)
    q1_resize = q1.resize((1024, 640))
    q1_arr = np.array(q1_resize)
    return q1_arr

def normalize(array): 
    # takes in array with dimension of the array is [480, 1024]
    # returns an array with dimensios [480, 1024]
    if np.max(array) - np.min(array) == 0:
        norm_mat = np.zeros((array.shape[0], array.shape[1]))
    else:
        norm_mat = (array - np.min(array))/(np.max(array)-np.min(array))   
    return norm_mat

def concatArrays( arr1, arr2): 
    # takes in array with dimension of the array is [1, 480, 1024, 1]
    # outputs an array with dimenstion of array [1, 480, 1024, 1] 
    arr1= np.vstack([arr1, np.zeros(arr1.shape[1])+1])       
    concat_arr = np.vstack([arr1, arr2])
    concat_arr = np.pad(concat_arr, ((1, 1), (1, 1)), mode='constant', constant_values=1)
    return concat_arr 

def create_gaussian(loc, sigma, img_size):
    loc_torch,sigma_torch=torch.from_numpy(np.array(loc)),torch.from_numpy(np.array(sigma))
    loc = loc_torch.double()
    sigma = sigma_torch.double()
    x_range = torch.range(0,int(img_size[0])-1,1)
    y_range = torch.range(0,int(img_size[1])-1,1)
    c = torch.stack(torch.meshgrid(x_range, y_range), dim=-1).double()
    sqr_dist =((c[:,:,0]-loc[0])**2) + ((c[:,:,1]-loc[1])**2)
    dist = torch.sqrt(sqr_dist)
    gaussian = torch.exp(-(torch.div(dist,(sigma)**2)))
    return gaussian

def create_mask_pytorch(image,locs,sigma):
    img_size = [int(image.shape[0]),int(image.shape[1])]
    attention_images = []
    image = image
    gaussian = create_gaussian([locs[0],locs[1]], sigma, img_size)
    hsv = cv2.cvtColor(image,cv2.COLOR_RGB2HSV)
    hsv_torch=torch.from_numpy(hsv)
    hsv_torch=hsv_torch.double()
    adjusted_hsv = torch.stack([hsv_torch[:,:,0],hsv_torch[:,:,1],hsv_torch[:,:,2]*gaussian],dim =2)
    adjusted_hsv=adjusted_hsv.numpy()
    adjusted_hsv = np.uint8(adjusted_hsv)
    adjusted_rgb = cv2.cvtColor(adjusted_hsv,cv2.COLOR_HSV2RGB)
    adjusted_rgb_torch=torch.from_numpy(adjusted_rgb)
    return adjusted_rgb_torch

def mpreward(coord, pred, margin = 0):
    img = np.zeros((640,1024))
    for li in range(len(coord)):
        gtymin = coord[li][2]
        gtxmin = max(coord[li][0],0)
        gtymax = coord[li][3]
        gtxmax = max(coord[li][1],0) 
        
        gtcoordinates = list((itertools.product(range(gtxmin,gtxmax),range(gtymin,gtymax))))
        gtcoordinates = set(gtcoordinates)
        
        predxmin,predymin,predh,predw = pred[0],pred[1],pred[2],pred[3]
        predcoordinates =list((itertools.product(range(predxmin,predxmin+predw),range(predymin,predymin+predh))))
        predcoordinates = set(predcoordinates)
        
        intersec = gtcoordinates.intersection(predcoordinates)  
        
        img[gtymin: gtymax, gtxmin : gtxmax] = 0.5
        
        if(intersec != set()):
            intersec = np.array(list(intersec))
            xmin,xmax,ymin,ymax = min(intersec[:,0]),max(intersec[:,0]),min(intersec[:,1]),max(intersec[:,1])
            img[ymin:ymax+1, xmin:xmax+1] = 1 
       
    img = Image.fromarray(np.uint8(img*255))
    imgs = np.array(img.resize((63, 39)))/255.0
        
    return imgs

def random_locs(batch_size, w = 100, h = 80):
    x_w = np.zeros([batch_size], dtype = int) + w
    y_h = np.zeros([batch_size], dtype = int) + h
    x = np.random.uniform(0,1024,batch_size).astype(int)
    y = np.random.uniform(0,640,batch_size).astype(int)
#    x,y = random.sample(range(0, 1024), batch_size),random.sample(range(0, 640), batch_size)
    locs = [i for i in zip(x,y,x_w,y_h)]
    return locs

def impute_data (path):
    a1 = np.array(Image.open(path))
    a2 = a1
    a2[0] = a1[1]
    a2[-2] = a1[-3]
    a2[-1] = a1[-3]
    return a2

class Subset(Dataset):
  
    def __init__(self, dataset, indices):
        self.dataset = dataset
        self.indices = indices

    def __getitem__(self, idx):
        return self.dataset[self.indices[idx]]

    def __len__(self):
        return len(self.indices)



def split_dataset(dataset, lengths):

    if sum(lengths) != len(dataset):
        raise ValueError("Sum of input lengths does not equal the length of the input dataset!")

    indices =list(range(0,sum(lengths)))
    return [Subset(dataset, indices[offset - length:offset]) for offset, length in zip(_accumulate(lengths), lengths)]

class Mpboxes(Dataset):
    def __init__(self,data_path,gt_path,sigma):
        super(DatasetProcessing).__init__()
        self.data_path = data_path
        self.sigma=sigma
        loc_w = 150
        loc_h = 100
        self.gt_path= gt_path
        self.data_dict = traffic_signal_data_multiple(self.data_path,self.gt_path)
        self.img_names=list(self.data_dict.keys())
        self.coords=list(self.data_dict.values())
        self.loc_b =random_locs(len(self.data_dict), loc_w, loc_h)
    def __getitem__(self, index):
        imgs=impute_data(self.data_path + '/' + str(self.img_names[index]))/255.0
        ocvimg=cv2.imread(self.data_path + '/' + str(self.img_names[index]))
        if((index+1)!=len(self.data_dict)):
            ocvimgn=impute_data(self.data_path + '/' + str(self.img_names[index+1]))/255.0
            ocvimg_next=cv2.imread(self.data_path + '/' + str(self.img_names[index+1]))
            img_rwds=mpreward(self.coords[index+1], self.loc_b[index+1])
            loc_c_next=self.loc_b[index+1]
        else:
            ocvimg_next=cv2.imread(self.data_path + '/' + str(self.img_names[0]))
            ocvimgn=impute_data(self.data_path + '/' + str(self.img_names[0]))/255.0
            img_rwds=mpreward(self.coords[0], self.loc_b[0])
            loc_c_next=self.loc_b[0]
        ocvimg_rgb=cv2.cvtColor(ocvimg,cv2.COLOR_BGR2RGB)
        ocvimg_rgb_next=cv2.cvtColor(ocvimg_next,cv2.COLOR_BGR2RGB)
        ocvimg_hsv=cv2.cvtColor(ocvimg_rgb,cv2.COLOR_RGB2HSV)
        loc_c=self.loc_b[index]
        img_rwds = np.reshape(np.array(img_rwds), [39, 63, 1])
        imgs = np.array(imgs)
        img_size=[imgs.shape[0],imgs.shape[1]]
        loc_c = np.array(loc_c)
        loc_c_next = np.array(loc_c_next)
        sigma=np.array(self.sigma)
        imgs_torch,loc_c_torch,loc_c_next_torch,sigmat,torch_ocvimgn,img_rwds_torch=torch.from_numpy(imgs),torch.from_numpy(loc_c),torch.from_numpy(loc_c_next),torch.from_numpy(sigma),torch.from_numpy(ocvimgn),torch.from_numpy(img_rwds)
        torch_mask=create_mask_pytorch(ocvimg_rgb,loc_c,self.sigma)
        torch_mask_next=create_mask_pytorch(ocvimg_rgb_next,loc_c_next_torch,self.sigma)
        imgs_torch,img_rwds_torch,torch_mask,torch_mask_next,torch_ocvimgn=imgs_torch.permute(2,0,1),img_rwds_torch.permute(2,0,1),torch_mask.permute(2,0,1),torch_mask_next.permute(2,0,1),torch_ocvimgn.permute(2,0,1)
        return imgs_torch,img_rwds_torch,loc_c_torch,torch_mask,torch_mask_next,torch_ocvimgn
    def __len__(self):
        return len(self.img_names[:len(self.img_names)])



         

